# DVA Chat Bot

The chat bot control is [Telerik's Chat control](https://docs.telerik.com/kendo-ui/api/javascript/ui/chat.html) for JQuery.

## Installing

````bash
npm i
````

### Start Dev Server

````bash
npm start
````

### Build Prod Version

````bash
npm run build
````

The build files are then created in ./build/. Where index.html is the chat bot HTML for running in isolation. And host-page.html is an example page of how a site can call the bot.

## Notes

* The control [meets WCAG 2.1 AAA](https://docs.telerik.com/kendo-ui/accessibility/section-508-wcag).
* [Telerik's webpack example build](https://github.com/telerik/kendo-ui-npm-example)
