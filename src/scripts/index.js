
$ = require("jquery");

require('whatwg-fetch');

require("@progress/kendo-ui/js/kendo.chat.js");

// require('../scripts/lib/showdown.js');
showdown = require('showdown');

// require('markdown-it');

// require('../scripts/lib/directLine.js');
DirectLine = require('botframework-directlinejs');

// require('./lib/adaptivecards.js');
AdaptiveCards = require('adaptivecards');

require('./chatbot-loader.js');

require("../styles/chat-bot.scss");

