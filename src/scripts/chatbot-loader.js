$(document).ready(function () {


  // if (window.jQuery) {
  //   // jQuery is loaded  
  //   console.log("Yeah Jquery");
  // } else {
  //   // jQuery is not loaded
  //   console.log("Naaa Jquery");
  // }

  // console.log(showdown);


  showdown.extension('targetlink', function () {
    return [{
      type: 'html',
      filter: function (text) {
        return ('' + text).replace(/<a\s+href=/gi, '<a target="_blank" href=');
      }
    }];
  });

  window.converter = new showdown.Converter({
    extensions: ['targetlink']
  });

  var MD_MESSAGE = kendo.template(
    '<div class="k-message">' +
    '<div class="k-bubble">' +
    '#= window.converter.makeHtml(text) #' +
    '</div>' +
    '</div>'
  );

  kendo.chat.registerTemplate("message", MD_MESSAGE);

  // API_URL is set in webpack config
  window.fetch(API_URL, { method: 'POST' })
    .then(function (res) {
      return res.json();
    })
    .then(function (json) {
      var token = json.token;
      var chat = $("#chat").kendoChat({
        post: function (args) {
          agent.postMessage(args);
        }
      }).data("kendoChat");

      var agent = new DirectLineAgent(chat, token);

      // wake up the bot
      agent.postMessage({ value: 'requestWelcomeDialog', type: 'event', from: { id: 'client-ui', name: 'client-ui' }, name: 'requestWelcomeDialog' });

    });

});


var AdaptiveCardComponent = kendo.chat.Component.extend({
  init: function (options, view) {
    kendo.chat.Component.fn.init.call(this, options, view);

    var adaptiveCard = new AdaptiveCards.AdaptiveCard();

    adaptiveCard.hostConfig = new AdaptiveCards.HostConfig({
      fontFamily: "Segoe UI, Helvetica Neue, sans-serif"
    });

    adaptiveCard.parse(options);

    var bodyElement = $("<div>").addClass("k-card-body").append(adaptiveCard.render());
    this.element.addClass("k-card").append(bodyElement);
  }
});

kendo.chat.registerComponent("application/vnd.microsoft.card.adaptive", AdaptiveCardComponent);



window.DirectLineAgent = kendo.Class.extend({
  init: function (chat, token) {

    this.chat = chat;
    // this.iconUrl = "content/botImage.png";
    this.agent = new DirectLine.DirectLine({ token: token, domain: "https://australia.directline.botframework.com/v3/directline" });
    this.agent.activity$.subscribe($.proxy(this.onResponse, this));
  },

  postMessage: function (args) {
    var postArgs = {
      text: args.text,
      type: args.type,
      timestamp: args.timestamp,
      from: args.from
    };

    this.agent.postActivity(postArgs)
      .subscribe();
  },

  onResponse: function (response) {
    if (response.from.id === this.chat.getUser().id) {
      return;
    }

    // response.from.iconUrl = this.iconUrl;

    this.renderMessage(response);
    if ("attachments" in response) {
      this.renderAttachments(response);
    }
    this.renderSuggestedActions(response.suggestedActions);
  },

  renderMessage: function (message) {
    // console.log('message');
    // console.log(message);
    // test for markdown telephone numbers
    // message.text = "call us on [1800 550 445](tel:1800550445)";


    if (message.text || message.type == "typing") {
      this.chat.renderMessage({
        type: "message",
        text: message.text
      }, message.from);
    }
  },


  renderAttachments: function (data) {
    this.chat.renderAttachments(data, data.from);
  },

  renderSuggestedActions: function (suggestedActions) {
    var actions = [];

    if (suggestedActions && suggestedActions.actions) {
      actions = suggestedActions.actions;
    }

    this.chat.renderSuggestedActions(actions);
  }
});
