const Path = require('path');
const Webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const CopyWebpackPlugin = require('copy-webpack-plugin');
// const UglifyJS = require("uglify-es");

const API_URL = JSON.stringify('https://neuterbot-client-dev.azurewebsites.net/api/directlineToken');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'cheap-eval-source-map',
  output: {
    chunkFilename: 'js/[name].chunk.js'
  },
  devServer: {
    inline: true,
    // port: 9001 // if you'd like to run on a different port
  },
  plugins: [
    new Webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
      'API_URL': API_URL
    }),
    new CopyWebpackPlugin([
      {
        from: Path.resolve(__dirname, '../src/scripts/va-include.js'),
        to: '../build/shell/js/va-include.js',
        transform(content) {
          return content.toString()
            .replace('INJECTED_CSS_URL', 'styles/chat-bot-control.css')
            .replace('INJECTED_BOT_CLIENT_URL', 'https://neuterbot-client-dev.azurewebsites.net');
        },
      },
      {
        from: Path.resolve(__dirname, '../public/images/botimage.svg'),
        to: '../build/images/botimage.svg'
      }
    ])
  ],
  module: {
    rules: [
      {
        test: /\.(js)$/,
        include: Path.resolve(__dirname, '../src'),
        enforce: 'pre',
        loader: 'eslint-loader',
        options: {
          emitWarning: true,
        }
      },
      {
        test: /\.(js)$/,
        include: Path.resolve(__dirname, '../src'),
        loader: 'babel-loader'
      },
      {
        test: /\.s?css$/i,
        use: ['style-loader', 'css-loader?sourceMap=true', 'sass-loader']
      }
    ]
  }
});
