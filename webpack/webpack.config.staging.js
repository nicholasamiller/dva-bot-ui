
const Path = require('path');
const Webpack = require('webpack');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const common = require('./webpack.common.js');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJS = require("uglify-es");

const API_URL = JSON.stringify('https://neuterbot-client-dev.azurewebsites.net/api/directlineToken');

module.exports = merge(common, {
  entry: './src/scripts/index.js',
  mode: 'production',
  devtool: 'source-map',
  stats: 'errors-only',
  bail: true,
  output: {
    path: Path.join(__dirname, '../build'),
    filename: './scripts/[name].js',
    chunkFilename: './scripts/[name].chunk.js'
  },
  plugins: [
    new Webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
      'API_URL': API_URL
    }),
    new Webpack.optimize.ModuleConcatenationPlugin(),
    new MiniCssExtractPlugin({
      filename: 'styles/chat-bot.css'
    }),
    //TODO:: replace 'someurl' with the real url for the staging bot
    new CopyWebpackPlugin([
      {
        from: Path.resolve(__dirname, '../src/scripts/va-include.js'),
        to: '../build/shell/js/va-include.js',
        transform(content) {
          return UglifyJS.minify(content.toString()
            .replace('INJECTED_CSS_URL', 'https://neuterbot-client-dev.azurewebsites.net/styles/chat-bot-control.css')
            .replace('INJECTED_BOT_CLIENT_URL', 'https://neuterbot-client-dev.azurewebsites.net'))
            .code;
        },
      }
    ])
  ],
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.s?css/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          'sass-loader',
          'postcss-loader'
          // MiniCssExtractPlugin.loader,
          // 'css-loader',
          // 'sass-loader'
        ]
      }
    ]
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false // set to true if you want JS source maps
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
});

